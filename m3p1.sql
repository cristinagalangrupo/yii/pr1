DROP DATABASE IF EXISTS m3p1;
CREATE DATABASE m3p1;
USE m3p1;

CREATE TABLE autores(
  id int AUTO_INCREMENT,
  nombre varchar(127),
  apellidos varchar(255),
  nacionalidad varchar(127),
  foto varchar(127),
  PRIMARY KEY(id)
  );
 
 CREATE TABLE libros(
  id_libros int AUTO_INCREMENT,
  titulo varchar(255),
  autor varchar(255),
  fecha date,
  foto varchar(127),
  PRIMARY KEY(id_libros)
  );
